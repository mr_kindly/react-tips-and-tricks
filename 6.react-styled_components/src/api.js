import axios from 'axios';

const productUrl = '/api/v1/products/';
const categoryUrl = '/api/v1/categories/';
const apiHost = 'https://remp-api.herokuapp.com';

const _categories = [{
  title: 'Category 1',
  id: 1,
}, {
  title: 'Category 2',
  id: 2
}];

const _products = [{
  title: 'Product 1',
  price: {
    value: 200,
    discount: 20,
    currency: '$'
  },
  description: 'Description product 1',
  imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png',
  id: 1,
  categoryId: 1
}, {
  title: 'Product 2',
  price: {
    value: 200,
    discount: 120,
    currency: '$'
  },
  description: 'Description product 2',
  imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png',
  id: 2,
  categoryId: 1
}, {
  title: 'Product 3',
  price: {
    value: 100,
    discount: 100,
    currency: '$'
  },
  description: 'Description product 3',
  imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png',
  id: 3,
  categoryId: 1
}, {
  title: 'Product 4',
  price: {
    value: 400,
    discount: 200,
    currency: '$'
  },
  description: 'Description product 4',
  imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png',
  id: 4,
  categoryId: 2
}]

export async function getProduct(id) {
  // const product = await axios.get(`${apiHost}${productUrl}${id}`).then(res => res.data);
  const product = _products.find(p => p.id == id);
  return product;
}

export async function getCategoryProducts(id) {
  return _products.filter(p => p.categoryId == id);//axios.get(`${apiHost}${categoryUrl}${id}/products`).then(res => res.data);
}

export async function getCategory(id) {
  //const category = await axios.get(`${apiHost}${categoryUrl}${id}`).then(res => res.data);
  const category = _categories.find(c => c.id == id);
  return category;
}

export async function getCategories() {
  return _categories;//await axios.get(`${apiHost}${categoryUrl}`).then(res => res.data);
}
