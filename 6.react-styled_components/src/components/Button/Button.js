import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
  background: ${props => props.success ? '#5cb85c' : '#fff'};
  color: ${props => props.success ? '#fff' : '#333'};

  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 2px;
  border-color: ${props => props.success ? '#4cae4c' : '#ccc'};
`;

export default Button;