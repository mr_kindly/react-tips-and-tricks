import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardImg, CardBody, CardTitle, CardText } from 'reactstrap';
import Price from './../price';


const ProductTile = (props) => {
    const { product, withShowMore } = props;
    return (<div>
        <Card className="text-center">
            <CardImg top width="100%" src={product.imgUrl} alt="watch" />
            <CardBody>
                <CardTitle>{product.title}</CardTitle>
                <Price price={product.price} />
                <CardText>{product.description}</CardText>
                <p>
                    <Button color="success">Add to bag</Button>
                    {withShowMore && <Link className="btn btn-default" role="button" to={`/product/${product.id}`}>Show more</Link>}
                </p>
            </CardBody>
        </Card>
    </div>);
};

export default ProductTile;
