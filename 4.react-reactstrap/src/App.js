import React, { Fragment } from 'react';
import CategoryPage from './pages/categoryPage';
import ProductPage from './pages/productPage';
import Navigation from './components/navigation';
import { HashRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

const App = () => (
        <Fragment>
            <header>
                <nav className="navbar navbar-inverse">
                    <div className="container">
                        <Navigation />
                        <div className="collapse navbar-collapse">
                            <ul className="nav navbar-nav navbar-right">
                                <li><a href="#"><span className="glyphicon glyphicon-user"></span></a></li>
                                <li><a href="#"><span className="glyphicon glyphicon-lock"></span></a></li>

                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div className="container">
                <Router>
                    <Switch>
                        <Route path='/category/:id' component={CategoryPage} />
                        <Route path='/product/:id' component={ProductPage} />
                        <Redirect from='*' to='/category/1' />
                    </Switch>
                </Router>
            </div>
        </Fragment>);

export default App;
