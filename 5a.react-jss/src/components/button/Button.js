import React from 'react'
import injectSheet from 'react-jss'

const styles = {
  button: {
    background: props => props.color,
    borderColor: '#4cae4c',
    color: '#fff',
    padding: '6px 12px',
    marginBottom: 0,
    fontSize: '14px',
    fontWeight: 400,
    lineHeight: 1.42857143,
    textAlign: 'center',
    whiteSpace: 'nowrap',
    verticalAlign: 'middle',
    backgroundImage: 'none',
    border: '1px solid transparent',
    borderRadius: '4px'
  },
  label: {
    fontWeight: 'bold',
  }
}

const Button = ({classes, children}) => (
  <button className={classes.button}>
    <span className={classes.label}>
      {children}
    </span>
  </button>
)

export default injectSheet(styles)(Button)