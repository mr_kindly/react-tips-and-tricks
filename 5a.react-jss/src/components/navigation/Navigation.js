import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCategories } from './../../store/actions/category';


class Navigation extends Component {
  componentDidMount() {
    this.getCategories();
  }

  getCategories() {
    this.props.fetchCategories();
  }

  render() {
    const { categories } = this.props;

    return !categories ? null : (
            <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed">
                    <span className="sr-only">Toggle navigation</span>
                </button>
                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul className="nav navbar-nav navbar-left">
                        {
                            categories.map(category => (<li key={category.id}>
                                    <a href={`#/category/${category.id}`}>{category.title}</a>
                                </li>))
                        }
                    </ul>
                </div>
            </div>
    );
  }
}

export default connect(
  ({ category }) => {
    const { categories } = category;

    return {
      categories,
    };
  },
  {
    fetchCategories,
  },
)(Navigation);
