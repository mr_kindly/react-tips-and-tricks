import React from 'react';
import { Link } from 'react-router-dom';
import Price from './../price';
import Button from './../button';


const ProductTile = (props) => {
  const { product, withShowMore } = props;
  return (
        <div className="thumbnail text-center">

            <img src={product.imgUrl} alt="watch" />
            <div className="caption">
                <h3>{product.title}</h3>
                <p>{product.description}</p>
                <Price price={product.price} />
                <p>
                    <Button color="#5cb85c">Add to bag</Button>
                    {withShowMore && <Link className="btn btn-default" role="button" to={`/product/${product.id}`}>Show more</Link>}
                </p>
            </div>
        </div>);
};

export default ProductTile;
