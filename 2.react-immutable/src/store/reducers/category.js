import {List} from 'immutable';
import actionTypes from './../../constants/actionTypes';


const defaultState = {
  categories: List(),
  products: List(),
  loading: false,
  error: false,
};

export default function (state = defaultState, { type, payload }) {
  switch (type) {
    case actionTypes.CATEGORIES_FETCH_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.CATEGORIES_FETCH_SUCCESS:
      return {
        ...state,
        categories: List(payload),
        loading: false,
        error: false,
      };
    case actionTypes.CATEGORIES_FETCH_FAIL:
      return {
        ...state,
        loading: false,
        error: true,
      };

    case actionTypes.PRODUCTS_FETCH_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.PRODUCTS_FETCH_SUCCESS:
      return {
        ...state,
        products: List(payload),
        loading: false,
        error: false,
      };
    case actionTypes.PRODUCTS_FETCH_FAIL:
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      return state;
  }
}
